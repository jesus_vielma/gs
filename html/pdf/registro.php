<?php
require('fpdf17/fpdf.php');

class PDF extends FPDF
{
// Cabecera de p�gina
function Header()
{
	// Logo
	$this->Image('PL.png',10,8,33);
	// Arial bold 15
	$this->SetFont('Times','',15);
	// T�tulo
 $this->Ln(5); //Saltos de Linea
 // Movernos a la derecha
 $this->Cell(45);
 //Texto
	$this->Cell(60,10,'GNU de Venezuela',0,0,'C');
 $this->Ln(5);  //Saltos de Linea
 $this->Cell(45);
 $this->Cell(60,10,'Punto Libre M�rida',0,0,'C');
 $this->Image('gnu.png',165,9,33);
	// Salto de l�nea
	$this->Ln(25);
}

// Pie de p�gina
function Footer()
{
	// Posici�n: a 1,5 cm del final
	$this->SetY(-15);
	// Arial italic 8
	$this->SetFont('Times','I',8);
	// N�mero de p�gina
	$this->Cell(0,10,'P�gina '.$this->PageNo().'/{nb}',0,0,'C');
}
function Firma()
{
	// Posici�n: a 1,5 cm del final
	$this->SetY(-50);
	// Arial italic 8
	$this->SetFont('Times','B',13);
	// N�mero de p�gina
 
	$this->Cell(50,10,'Firma del receptor ',0,0);
 $this->Cell(70);
 $this->Cell(50,10,'Firma del Cliente',0,0);
}
}
include('../conexion.php');
$cod_soporte=$_GET['cod_soporte'];
$sql=mysql_query("SELECT  `dueno`.`nombre` ,  `dueno`.`apellido` ,  `dueno`.`nacionalidad` ,  `dueno`.`cedula` ,  `equipo`.`tipo_equipo` ,  `equipo`.`mem_ram` ,  `equipo`.`disco_duro` , `pc_portatil`.`serial_portatil` ,  `pc_portatil`.`modelo_portatil` ,  `pc_portatil`.`descripcion_portatil` ,  `pc_escritorio`.`descripcion_escritorio` ,  `pc_escritorio`.`color_case` , `canaima`.`serial_canaimita` ,  `canaima`.`modelo_canaimita` ,  `plantel`.`ubicacion_plantel`,`plantel`.`nombre_plantel` ,`plantel`.`grado_canaimita`, `soporte`.`cod_soporte`, `soporte`.`tipo_soporte`, `soporte`.`descripcion_soporte`,
                 Date_Format(`soporte`.`fecha_recepcion`,'%d-%m-%Y') AS fecha_recepcion, `soporte`.`responsable`
FROM dueno
INNER JOIN equipo ON  `equipo`.`cedula` =  `dueno`.`cedula` 
LEFT JOIN pc_portatil ON  `pc_portatil`.`id_equipo` =  `equipo`.`id_equipo` 
LEFT JOIN canaima ON  `canaima`.`id_equipo` =  `equipo`.`id_equipo` 
LEFT JOIN plantel ON `plantel`.`id_escuela`=`canaima`.`id_escuela`
LEFT JOIN pc_escritorio ON  `pc_escritorio`.`id_equipo` =  `equipo`.`id_equipo` 
INNER JOIN soporte ON  `soporte`.`cod_soporte` =  `equipo`.`cod_soporte` 
WHERE  `soporte`.`cod_soporte` ='$cod_soporte'");
$date=getdate();
// Creaci�n del objeto de la clase heredada
$pdf = new PDF('P','mm','Letter');
$pdf->SetMargins('30','30','30');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Cell(35);
$pdf->Cell(60,10,'Recepci�n de equipo para soporte t�cnico.',0,1,'');
$pdf->Ln(2);
while($row=mysql_fetch_array($sql))
{if($row['tipo_equipo']=='portatil')
 {$txt="Yo, ".utf8_decode($row['nombre'])." ".utf8_decode($row['apellido']).', con cedula de identidad '.$row['nacionalidad'].'-'.$row['cedula'].', hago entrega al Punto Libre M�rida y sus t�cnicos un equipo tipo: '.$row['tipo_equipo'].' con las siguientes caracteristicas de harware: Memoria Ram de: '.$row['mem_ram'].'. Disco duro de: '.$row['disco_duro'].'. Serial: '.$row['serial_portatil'].'. Modelo: '.$row['modelo_portatil'].'.';
 $txt.=' Con la siguiete descripci�n: '.utf8_decode($row['descripcion_portatil']).'.';
 $pdf->SetFont('Times','B','13');
$pdf->Cell(116);
$pdf->Cell(40,5,'Codigo del soporte '.$row['cod_soporte'].'',0,1,'R');
$pdf->Ln(2);
$pdf->SetFont('Times','',12);
$pdf->MultiCell(0,5,$txt,'J');
$pdf->Cell('0','5','Tipo de soporte: '.utf8_decode($row['tipo_soporte']),0,1,'L');
$soporte='Descripcion del soporte: '.utf8_decode($row['descripcion_soporte']);
$pdf->MultiCell(0,5,$soporte,'J');
$pdf->Cell('0','5','Responsable del soporte: '.utf8_decode($row['responsable']),0,1,'L');
$pdf->Cell('0','5','Fecha de recepci�n: '.utf8_decode($row['fecha_recepcion']),0,1,'L');
$pdf->Firma();
}
else
if($row['tipo_equipo']=='canaimita')
{$txt="Yo, ".utf8_decode($row['nombre'])." ".utf8_decode($row['apellido']).', con cedula de identidad '.$row['nacionalidad'].'-'.$row['cedula'].', hago entrega al Punto Libre M�rida y sus t�cnicos un equipo tipo: '.$row['tipo_equipo'].' con Serial No.: '.$row['serial_canaimita'].'. Ubicada en el municipio: '.$row['ubicacion_plantel'].'. Del plantel: '.$row['nombre_plantel'].'. Del '.$row['grado_canaimita'].'.';
 //$txt.=' Con la siguiete descripci�n: '.utf8_decode($row['descripcion_portatil']).'.';
 $pdf->SetFont('Times','B','13');
$pdf->Cell(116);
$pdf->Cell(40,5,'Codigo del soporte '.$row['cod_soporte'].'',0,1,'R');
$pdf->Ln(2);
$pdf->SetFont('Times','',12);
$pdf->MultiCell(0,5,$txt,'J');
$pdf->Cell('0','5','Tipo de soporte: '.utf8_decode($row['tipo_soporte']),0,1,'L');
$soporte='Descripcion del soporte: '.utf8_decode($row['descripcion_soporte']);
$pdf->MultiCell(0,5,$soporte,'J');
$pdf->Cell('0','5','Responsable del soporte: '.utf8_decode($row['responsable']),0,1,'L');
$pdf->Cell('0','5','Fecha de recepci�n: '.utf8_decode($row['fecha_recepcion']),0,1,'L');
$pdf->Firma();
}
else
{$txt="Yo, ".utf8_decode($row['nombre'])." ".utf8_decode($row['apellido']).', con cedula de identidad '.$row['nacionalidad'].'-'.$row['cedula'].', hago entrega al Punto Libre M�rida y sus t�cnicos un equipo tipo: '.$row['tipo_equipo'].' con las siguientes caracteristicas de harware: Memoria Ram de: '.$row['mem_ram'].'. Disco duro de: '.$row['disco_duro'].'.';
 $txt.=' Con la siguiete descripci�n: '.utf8_decode($row['descripcion_escritorio']).'.';
 $pdf->SetFont('Times','B','13');
$pdf->Cell(116);
$pdf->Cell(40,5,'Codigo del soporte '.$row['cod_soporte'].'',0,1,'R');
$pdf->Ln(2);
$pdf->SetFont('Times','',12);
$pdf->MultiCell(0,5,$txt,'J');
$pdf->Cell('0','5','Tipo de soporte: '.utf8_decode($row['tipo_soporte']),0,1,'L');
$soporte='Descripcion del soporte: '.utf8_decode($row['descripcion_soporte']);
$pdf->MultiCell(0,5,$soporte,'J');
$pdf->Cell('0','5','Responsable del soporte: '.utf8_decode($row['responsable']),0,1,'L');
$pdf->Cell('0','5','Fecha de recepci�n: '.utf8_decode($row['fecha_recepcion']),0,1,'L');
$pdf->Firma();
}
}
$pdf->Output('soporteNo_'.$cod_soporte,'I');
?>
