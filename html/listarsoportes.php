<?php
require('cabecera.php');
require('menu_lateral.php');
?>
				<div class="span9">
					<div class="hero-unit">
						<h3 class="text-center">Listado de Soportes</h3>
						<br>
							  <ul class="nav nav-tabs">
    <li class="active"><a href="#fecha" data-toggle="tab">Listar por Fecha</a></li>
    <li><a href="#mes" data-toggle="tab">Listar por Mes</a></li>
				<li class="disabled"><a href="#equipo" data-toggle="tab">Tipo de Equipo</a></li>
  </ul>
									<div class="tab-content">
										<div class="tab-pane active" id="fecha">
														<form method="post" action="listarsoporte_fecha.php">
						<div class="span12 text-center">
							<h4>Listado por fecha específica</h4>
							<div class="input-append">
								                <div class="input-append date form_date " data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
                    <input  type="text" value="" required>
                    <!--<span class="add-on"><i class="icon-remove"></i></span>
																				Con esta Linea Activo un boton para elimnar la fecha de alli-->
																				<span class="add-on"><i class="icon-calendar"></i></span>
																								<input type="hidden" id="dtp_input2" name="fecha" value="" />
							<button class="btn btn-primary" type="submit" ><i class="icon-list icon-white"></i> Listar</button>
							</div>
						</div>
					</div>
					</form>
										</div>
										<div class="tab-pane" id="mes">
											<form method="post" action="listarsoporte_mes.php">
												<div class="span12 text-center">
													<h4>Listado por Mes</h4>
													<div class="input-append">
														<select name="mes">
															<option value="01">Enero</option>
															<option value="02">Febrero</option>
															<option value="03">Marzo</option>
															<option value="04">Abril</option>
															<option value="05">Mayo</option>
															<option value="06">Junio</option>
															<option value="07">Julio</option>
															<option value="08">Agosto</option>
															<option value="09">Septiembre</option>
															<option value="10">Octubre</option>
															<option value="11">Noviembre</option>
															<option value="12">Diciembre</option>
														</select>
														<button class="btn btn-primary" type="submit"><i class="icon-list icon-white"></i> Listar</button>
												</div>
											</div>
										</div>
									</div>
					</div>
					</form>
					</div>
					</form>
				</div>

<?php
require('pie.php');
?>
