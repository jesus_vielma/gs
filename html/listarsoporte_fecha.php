<?php
require('cabecera.php');
require('menu_lateral.php');
?>
				<div class="span9">
					<div class="hero-unit">
						<h3 class="text-center">Listado de Soportes</h3>
                   <?php
																		$fecha=$_POST['fecha'];
																		echo "<p class='text-center'>Hechos en el día <strong>".$fecha. "</strong></p>"
																		?>
						<br>
						<form method="post" >
                    <table class="table table-hover table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Estado</th>
                                <th>Fecha de Registro</th>
                                <th>Nº de Registro</th>
                                <th>Cliente</th>
                                <th>Tipo de Equipo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Terminado</td>
                                <td>01/09/2013</td>
                                <td>00001</td>
                                <td>Jesús Vielma</td>
                                <td>Canaimita</td>
                            </tr>
                            <tr>
                                <td>Terminado</td>
                                <td>01/09/2013</td>
                                <td>00002</td>
                                <td>Pedro Peralta</td>
                                <td>Escritorio</td>
                            </tr>
                            <tr >
                                <td>Terminado</td>
                                <td>01/09/2013</td>
                                <td>00003</td>
                                <td>Engelbert Portillo</td>
                                <td>Portatil</td>
                            </tr>
                             <tr >
                                <td>Terminado</td>
                                <td>01/09/2013</td>
                                <td>00003</td>
                                <td>Pedro Perez</td>
                                <td>Escritorio</td>
                            </tr>
                              <tr >
                                <td>Terminado</td>
                                <td>01/09/2013</td>
                                <td>00003</td>
                                <td>Juan Delgado</td>
                                <td>Portatil</td>
                            </tr>
                               <tr >
                                <td>Terminado</td>
                                <td>01/09/2013</td>
                                <td>00003</td>
                                <td>Omar Lopez</td>
                                <td>Canamita</td>
                            </tr>
                        </tbody>
                    </table>
						  <div class="row-fluid text-center">
						<div class="span12">
								<button type="submit" class="btn btn-primary"><i class="icon-print icon-white"></i> Imprimir</button>
								<button type="submit" class="btn btn-primary"><i class="icon-download-alt icon-white"></i> Descargar</button>
								<button type="button" class="btn btn-primary"><i class="icon-envelope icon-white"></i> Enviar</button>
						</div>
						</div>
                  </form>
					</div>
				</div>
<?php
require('pie.php');
?>
