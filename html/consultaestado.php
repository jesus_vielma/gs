<?php
require('cabecera.php');
require('menu_lateral.php');
?>
				<div class="span9">
					<div class="hero-unit">
						<h3 class="text-center">Consultar el estado de un soporte</h3>
						<br>
						<form method="post" action="" autocomplete="off">
						<div class="well">
						<div class="row-fluid">
						<div class="span12 text-center">
								<div class="input-prepend input-append">
								<span class="add-on">Código de Registro</span>
									<input type="text" name="cod_reporte" placeholder="Código de registro" title="El código de registro tiene la siguente forma ..."/>
								<button class="btn btn-primary" type="submit"><i class="icon-search icon-white"></i> Buscar</button>
								</div>
						</div>
						</div>
                  </div>
                </form>
						<!--<h4 class="text-center">Ó</h4>
                <form method="post" >
						<div class="well">
						<div class="row-fluid">
						<div class="span12 text-center">
								<div class="input-prepend input-append">
								<span class="add-on">Número de Cédula</span>
                        <input type="text" name="nro_cedula" placeholder="Cédula del cliente" title="Coloque el número de cédula sin puntos."/>
								<button class="btn btn-primary" type="submit"><i class="icon-search icon-white"></i> Buscar</button>
								</div>
						</div>
						</div>
                </div>
                </form>-->
						<h4 class="text-center">Ó</h4>
                <form method="post" action="consultaestado_fecha.php">
						<div class="well">
						<div class="row-fluid">
						<div class="span12 text-center">
								<div class="input-prepend input-append">
								<span class="add-on">Fecha de Registro</span>
               <div class="input-append date form_date " data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                    <input  type="text" value="" required>
                    <!--<span class="add-on"><i class="icon-remove"></i></span>
																				Con esta Linea Activo un boton para elimnar la fecha de alli-->
																				<span class="add-on"><i class="icon-calendar"></i></span>
																								<input type="hidden" id="dtp_input2" name="fecha" value="" />
                </div>
								<button class="btn btn-primary" type="submit" ><i class="icon-search icon-white"></i> Buscar</button>
								</div>
						</div>
						</div>
                </div>
                </form>
					</div>
				</div>
<?php
require('pie.php');
?>
