<?php
require('cabecera.php');
require('menu_lateral.php');
?>
				<div class="span9">
					<div class="hero-unit">
						<h3 class="text-center">Registo de Soporte</h3>
						<br>
						  
					 <form method="post" action="registra_soporte.php" autocomplete="off">
					 <div class="row-fluid">
						  <div class="span12 text-center barra">
								<span>Escoja El tipo de equipo</span>
						  </div>
					 </div>
						<div class="row-fluid">
						  <div class="span12 text-center">
							<div data-toggle="buttons-radio" >
								<label class="radio inline">
								<span class="btn btn-primary">Portatil</span> <input type="radio" name="equipo" value="portatil" onclick="escuela.disabled=true;
								grado.disabled=true; muni.disabled=true;	marca.disabled=false; dd.disabled=false; ram.disabled=false; modelo.disabled=false; color.disabled=false;
								serial.disabled=false; tipo_soporte[6].disabled=true;" required hidden title="Debe seleccionar una opcion"/>
								</label>
								
								<label class="radio inline">
											<span class="btn btn-primary">Canaimita</span> <input type="radio" name="equipo" value="canaimita"
								onclick="escuela.disabled=false; grado.disabled=false; muni.disabled=false;
								marca.disabled=true, dd.disabled=true; ram.disabled=true; modelo.disabled=false; color.disabled=true;
								serial.disabled=false; tipo_soporte[6].disabled=false;" hidden/>
								</label>
								<label class="radio inline">
								<span class="btn btn-primary">Escritorio</span> <input type="radio" name="equipo" value="escritorio" onclick="escuela.disabled=true; 
								grado.disabled=true; muni.disabled=true;  dd.disabled=false; ram.disabled=false; modelo.disabled=false; color.disabled=false;
								serial.disabled=true; tipo_soporte[6].disabled=true;" hidden/>
								</label>
						  </div>
								</div>
								</div>
						<!--
						<div class="row-fluid">
						  <div class="span12 text-center btn-primary">
								<span>Datos del Propietario</span>
						  </div>
						</div>
						  <div class="row-fluid">
								<div class="span12">
									 <div class="span4">
										 <span>Cédula:</span> <input type="text" name="cedula" required placeholder="C.I. 00.000.000" title="Solo puede insertar números">
									 </div>
									 <div class="span4">
										  <span>Nombre:</span> <input type="text" name="name">
									 </div>
									 <div class="span4">
										  <span>Apellido:</span> <input type="text" name="last-name">
									 </div>
								</div>
						  </div>
						  <div class="row-fluid">
								<div class="span12">
									 <div class="span4">
										  <span>Teléfono:</span> <input type="tel" name="phone" maxlength="7" size="18"title="Debes Incluir el codigo del Operador o área!" placeholder="0000-0000000"/>
									 </div>
									 <div class="span6">
										  <span>Correo Electronico</span> <input type="email" name="e-mail" placeholder="ejemplo@ejemplo.com">
									 </div>
								</div>
						  </div>-->
						  <div class="row-fluid">
								<div class="span12 text-center barra">
									 <span>En caso de ser Una Canaimita</span>
								</div>
						  </div>
						 <!-- <div class="row-fluid">
								<div class="span12">
									 <div class="span6">
										  <span>Cédula del <br>Representante</span> <input type="text" name="cedula_repre" disabled required />
									 </div>
									 <div class="span6">
										  <span>Nombre del <br>Representante</span> <input type="text" name="repre" disabled required/>
									 </div>
								</div>
								</div>-->
						  <div class="row-fluid">
								<div class="span12">
									 <div class="span4">
										  <span>Institución</span> <input class="input-block-level" type="text" name="escuela" disabled required />
									 </div>
									 <div class="span4">
										  <span>Grado o año</span> <!--<input type="text" name="grado" disabled required />-->
													<select class="input-block-level"name="grado" disabled>
										<optgroup label="Primaria">
											<option value="1er grado">Primer Grado</option>
											<option value="2do grado">Segundo Grado</option>
											<option value="3er grado">Tercer Grado</option>
											<option value="4to grado">Cuarto Grado</option>
											<option value="5to grado">Quinto Grado</option>
											<option value="6to grado">Sexto Grado</option>
										</optgroup>
										<optgroup label="Bachillerato">
											<option value="1er año">Primer año</option>
											<option value="2do año">Segundo año</option>
											<option value="3er año" disabled>Tercer Grado</option>
											<option value="4to año" disabled>Cuarto Grado</option>
											<option value="5to año" disabled>Quinto Grado</option>
										</optgroup>
									</select>
									 </div>
										<div class="span4">
											<span>Municipio</span> <input class="input-block-level"type="text" name="muni" disabled required />
									  </div>
								</div>
						  </div>
						  <div class="row-fluid">
								<div class="span12 text-center barra">
									 <span>Datos del Equipo</span>
								</div>
						  </div>
						  <div class="row-fluid">
								<div class="span12">
									 <div class="span4">
										  <span>Serial</span> <input class="input-block-level" type="text" name="serial" required />
									 </div>
									 <div class="span4">
										  <span>Disco Duro</span> <input class="input-block-level"type="text" name="dd" title="Especifique la cantidad en Mb ó Gb" required/>
									 </div>
									 <div class="span4">
										  <span>Memoria RAM</span> <input class="input-block-level" type="text" name="ram" required/>
									 </div>
								</div>
						  </div>
						  <div class="row-fluid">
								<div class="span12">
									 <div class="span4">
										  <span>Marca</span> <input class="input-block-level" type="text" name="marca" title="Escriba la Marca de equipo" />
									 </div>
									 <div class="span4">
										  <span>Modelo</span> <input class="input-block-level" type="text" name="modelo" title="Escriba el Color del equipo" class="span5"/>
									 </div>
									 <div class="span4">
										  <span>Color</span>
										  <select class="input-block-level" name="color">
												<option value="amarillo">Amarillo</option>
												<option value="azul">Azul</option>
												<option value="rojo">Rojo</option>
												<option value="negro">Negro</option>
												<option value="blanco">Blanco</option>
												<option value="otro">Otro</option>
										  </select>
									 </div>
								</div>
						  </div><!--
								<div class="row-fluid">
									Componetes extras<br/>
									<div class="span12">
										<span>Tarjeta de Video</span> <input type="checkbox" name="extras[]" value="tarjeta de video" />
										<span>Tarjeta de red</span> <input type="checkbox" name="extras[]" value="tarjeta de video" />
										<span>Tarjeta de Video</span> <input type="checkbox" name="extras[]" value="tarjeta de video" />
										<span>Tarjeta de Video</span> <input type="checkbox" name="extras[]" value="tarjeta de video" />
								</div>-->
						  <div class="row-fluid">
								<div class="span12">
									 <span>Otras características</span><br>
									 <textarea class="input-block-level" placeholder="Indique otras caracticas que sean de ayuda para la identificación de equipo" name="descripcion" maxlength="150" cols="50" rows="3"></textarea>
								</div>
						  </div>
						  <div class="row-fluid">
								<div class="span12 text-center barra">
									 <span>Tipo de Soporte</span>
								</div>
						  </div>
						  <div class="row-fluid">
								<div class="span12">
									 <div class="span6">
										  <span>Tipo de Soporte</span>
										  <select class="input-block-level" name="tipo_soporte">
										<optgroup label="Falla">
											<option value="No enciende">No Enciende</option>
											<option value="No entre en el sistema">Enciende pero no entra al Sistema Operativo</option>
											<option value="No se ve la pantalla">No se ve la Pantalla</option>
											<option value="otros" title="Indique la falla que el cliente le reporta">Otros</option>
										</optgroup>
										<optgroup label="Software">
											<option value="Actualizacion de software" title="Indique que tipo de software">Actualización del Software</option>
											<option value="Instalar S.O." title="Especifique cual">Instalación de Sistema Operativo</option>
											<option value="Actualizar contenidos educativos" title="Indique el grado o año"> Actualización de Contenidos Educativos</option>
											<option>Otros</option>
										</optgroup>
									</select>
									 </div>
										<div class="span6">
									 <span >Descripción</span><br>
									 <textarea class="input-block-level" name="descripcion_soporte" cols="35" rows="5" title="Escriba una breve descripcion de lo que el cliente exprese" ></textarea>
									 
								</div>
						  </div>
								</div>
						  <div class="row-fluid">
									 <div class="span4">
										 <span>Fecha de recepción</span>
	                <div class="input-append date form_date " data-date="" data-date-format="dd MM yyyy" data-link-field="ingreso" data-link-format="yyyy-mm-dd">
                    <input class="input-block-level" type="text" value="" required >
                    <!--<span class="add-on"><i class="icon-remove"></i></span>
																				Con esta Linea Activo un boton para elimnar la fecha de alli-->
																				<span class="add-on"><i class="icon-calendar"></i></span>
																								<input type="hidden" id="ingreso" name="fecha_recepcion" value="" />
                </div>
            </div>
										<!--
									  <div class="span4">
										 <span>Fecha de Entrega</span><br/> 
	                <div class="input-append date form_date " data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
                    <input  type="text" value="" required>
                    <!--<span class="add-on"><i class="icon-remove"></i></span>
																				Con esta Linea Activo un boton para elimnar la fecha de alli
																				<span class="add-on"><i class="icon-calendar"></i></span>
																								<input type="hidden" id="dtp_input2" name="fecha_entrega" value="" />
                </div>
								</div>-->

										<div class="span4">
										<span>Seleccione un técnico</span><br>
										<?php
												include('conexion2.php');
												conexion2();
										  $consulta="SELECT * FROM  persona where nivel='tecnico' order by especialidad DESC;";
										  $tecnicos=mysql_query($consulta);
										echo "<select class='input-block-level' name='tecnico'>";
										echo "<option value=''>Seleccione un técnico</option>";
										while ($fila=mysql_fetch_array($tecnicos,0)){
										    echo utf8_encode("<option value='".$fila['nombre_usuario']."' title='".$fila['especialidad']."'>".$fila['nombre_usu']." ".$fila['apellido_usu']."</option>");
										    }
										echo "</select>";
										?>
									</div>
								</div>
						  <br><br>
														<?php
$cedula=$_POST['cedula'];
echo "<input type='hidden' name='cedula' value='$cedula'>";
?>
						  <div class="row-fluid">
								<div class="span12 text-center">
									 <a class="btn btn-primary" href="#adveterncia" data-toggle="modal" role="button"><i class="icon-file icon-white"></i> Guardar</a>
									 <button type="reset" class="btn btn-danger"><i class="icon-remove icon-white"></i> Cancelar</button>
								</div>
						  </div>
<div id="adveterncia" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header ">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Advertencia de Seguridad</h3>
  </div>
  <div class="modal-body alert alert-block">
    <strong><p>¿Esta totalmente seguro que desea guardar los datos introducidos anterioremente.?</p></strong>
	 <p>Antes de realizar el almacenado de los datos por favor verifique que los datos son los correctos y proceda a hacer el guardado de los
	 datos para su posterior impresión.</p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    <button type="submit" class="btn btn-primary"><i class="icon-file icon-white"></i> Guardar</button>
  </div>
</div>
						</form>

					</div>
					
				</div>
				
			</div>
			
		</div>
																											<!-- Modal -->
	<!-- Modal 2-->
<div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Se ha guardado con Exito!</h3>
  </div>
  <div class="modal-body alert-success">
	<p>¡En hora buena! Se han guardado exitosamente.</p>
	<div class="progress progress-success progress-striped">
		<div class="bar" style="width: 100%"></div>
	</div>
	<p>Ahora puede hacer la impresión de la planilla para el cliente y la planilla para el Punto Libre.
	Recuerde al Cliente que debe esperar por la planilla, puesto que es necesaria para poder hacer entrega del equipo.</p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
	 <button type="submit" class="btn btn-primary"><i class="icon-print icon-white"></i> Imprimir</button>
  </div>
</div>
<?php
require('pie.php');
?>
