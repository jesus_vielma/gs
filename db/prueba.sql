DROP SCHEMA IF EXISTS `prueba` ;
CREATE SCHEMA IF NOT EXISTS `prueba` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci ;
USE `prueba` ;

-- Tabla persona
CREATE TABLE IF NOT EXISTS `persona` (
  `nombre_usuario` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_usu` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_usu` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `especialidad` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `nivel` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`nombre_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcado de datos para la tabla `persona`

INSERT INTO `persona` (`nombre_usuario`, `password`, `nombre_usu`, `apellido_usu`, `especialidad`, `nivel`) VALUES
('andi', '', 'Andy ', 'Torres', 'Canaimitas', 'Técnico'),
('chucho', 'b79667e473a4ea59842ef30487ca97b2', 'Jesus', 'Vielma', '0', 'ayudante'),
('j_vielma', '314c37314000fd7a35ecaa14860c84ce', 'Jesús', 'Vielma', 'Contenido educativo', 'Técnico'),
('juan_23', '', 'Juan', 'Peña', 'Canaimitas', 'Técnico'),
('masc1293', '', 'Miguel', 'Sanabria', 'Canaimitas', 'Técnico'),
('pedro_perez', '', 'Pedro', 'Perez', 'laptops', 'Técnico');
