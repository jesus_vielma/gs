SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `gs_db` ;
CREATE SCHEMA IF NOT EXISTS `gs_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci ;
USE `gs_db` ;

-- -----------------------------------------------------
-- Table `gs_db`.`dueno`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gs_db`.`dueno` ;

CREATE TABLE IF NOT EXISTS `gs_db`.`dueno` (
  `nacionalidad` CHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `cedula` INT(11) NOT NULL,
  `nombre` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `apellido` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `telefono` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `correo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  PRIMARY KEY (`cedula`),
  UNIQUE INDEX `cedula_UNIQUE` (`cedula` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `gs_db`.`soporte`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gs_db`.`soporte` ;

CREATE TABLE IF NOT EXISTS `gs_db`.`soporte` (
  `cod_soporte` INT(11) NOT NULL AUTO_INCREMENT,
  `tipo_soporte` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `descripcion_soporte` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `fecha_recepcion` DATE NULL DEFAULT NULL,
  `fecha_entrega` DATE NULL DEFAULT NULL,
  `responsable` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  PRIMARY KEY (`cod_soporte`),
  UNIQUE INDEX `cod_soporte_UNIQUE` (`cod_soporte` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `gs_db`.`equipo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gs_db`.`equipo` ;

CREATE TABLE IF NOT EXISTS `gs_db`.`equipo` (
  `id_equipo` INT(11) NOT NULL AUTO_INCREMENT,
  `disco_duro` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `tipo_equipo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `mem_ram` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `cedula` INT(11) NOT NULL,
  `cod_soporte` INT(11) NOT NULL,
  PRIMARY KEY (`id_equipo`),
  UNIQUE INDEX `id_equipo_UNIQUE` (`id_equipo` ASC),
  INDEX `fk_equipo_dueno_idx` (`cedula` ASC),
  INDEX `fk_equipo_soporte1_idx` (`cod_soporte` ASC),
  CONSTRAINT `fk_equipo_dueno`
    FOREIGN KEY (`cedula`)
    REFERENCES `gs_db`.`dueno` (`cedula`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_equipo_soporte1`
    FOREIGN KEY (`cod_soporte`)
    REFERENCES `gs_db`.`soporte` (`cod_soporte`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `gs_db`.`plantel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gs_db`.`plantel` ;

CREATE TABLE IF NOT EXISTS `gs_db`.`plantel` (
  `id_escuela` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre_plantel` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `ubicacion_plantel` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `grado_canaimita` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  PRIMARY KEY (`id_escuela`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `gs_db`.`canaima`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gs_db`.`canaima` ;

CREATE TABLE IF NOT EXISTS `gs_db`.`canaima` (
  `serial_canaimita` INT(11) NOT NULL,
  `modelo_canaimita` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `id_equipo` INT(11) NOT NULL,
  `id_escuela` INT(11) NOT NULL,
  UNIQUE INDEX `serial_canaimita_UNIQUE` (`serial_canaimita` ASC),
  PRIMARY KEY (`id_equipo`),
  INDEX `fk_canaima_plantel1_idx` (`id_escuela` ASC),
  CONSTRAINT `fk_canaima_equipo1`
    FOREIGN KEY (`id_equipo`)
    REFERENCES `gs_db`.`equipo` (`id_equipo`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_canaima_plantel1`
    FOREIGN KEY (`id_escuela`)
    REFERENCES `gs_db`.`plantel` (`id_escuela`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `gs_db`.`estado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gs_db`.`estado` ;

CREATE TABLE IF NOT EXISTS `gs_db`.`estado` (
  `estado` VARCHAR(11) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `soporte_realizado` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `fecha_actualizacion` DATE NOT NULL,
  `cod_soporte` INT(11) NOT NULL,
  PRIMARY KEY (`cod_soporte`),
  CONSTRAINT `fk_estado_soporte1`
    FOREIGN KEY (`cod_soporte`)
    REFERENCES `gs_db`.`soporte` (`cod_soporte`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `gs_db`.`pc_escritorio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gs_db`.`pc_escritorio` ;

CREATE TABLE IF NOT EXISTS `gs_db`.`pc_escritorio` (
  `color_case` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `descripcion_escritorio` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `id_equipo` INT(11) NOT NULL,
  PRIMARY KEY (`id_equipo`),
  CONSTRAINT `fk_pc_escritorio_equipo1`
    FOREIGN KEY (`id_equipo`)
    REFERENCES `gs_db`.`equipo` (`id_equipo`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `gs_db`.`pc_portatil`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gs_db`.`pc_portatil` ;

CREATE TABLE IF NOT EXISTS `gs_db`.`pc_portatil` (
  `serial_portatil` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `modelo_portatil` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `descripcion_portatil` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NOT NULL,
  `id_equipo` INT(11) NOT NULL,
  UNIQUE INDEX `serial_portatil_UNIQUE` (`serial_portatil` ASC),
  PRIMARY KEY (`id_equipo`),
  CONSTRAINT `fk_pc_portatil_equipo1`
    FOREIGN KEY (`id_equipo`)
    REFERENCES `gs_db`.`equipo` (`id_equipo`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
